pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract issueNftTokens {

    struct Token{
        string name;
        uint price;
        uint age;
        uint health;
    }

    Token[] tokensArr;
    mapping(uint => uint) tokenOwner;
    
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);

      
    }

    modifier UniqueName (string name)
    {        
        bool containsSameName = false;
        for (uint8 i = 0;i<tokensArr.length;i++)
        {
            if (tokensArr[i].name == name)
            {
                containsSameName = true;
                break;
            }
        }
        require(!containsSameName, 103);
        _;
    }

    function createToken(string name, uint age, uint health) public UniqueName(name) {
        tvm.accept();
        tokensArr.push(Token(name, 0, age, health));
        uint key = tokensArr.length - 1;
        tokenOwner[key]=msg.pubkey();
    
    }

    modifier IsOwner (uint tokenId) {        
        require(msg.pubkey() == tokenOwner[tokenId], 101);
        _;
    }

    function setPrice(uint tokenId, uint price) public IsOwner(tokenId) {
    
        tvm.accept();
        tokensArr[tokenId].price = price;
    }
}
