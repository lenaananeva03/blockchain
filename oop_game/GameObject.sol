pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

import 'GameObjectInterface.sol';

contract GameObject is GameObjectInterface {
    uint private livesCount;
    address public destroyAddress;
    uint private protection;

    constructor(uint curProtection, uint curLivesCount) public
    {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        protection = curProtection;
        SetLivesCount(curLivesCount);
    }

    function SetLivesCount(uint set_lc) public
    {
        tvm.accept();
        livesCount = set_lc;
    }

    function GetLivesCount() public returns(uint)
    {
        return livesCount;
    }

    function GetProtectionPower() public returns(uint)
    {
        return protection;
    }

     function SetProtectionPower(uint curProtection) public
    {
        protection = curProtection;
    }

    function AcceptAttack(uint attack_power) public override
    {
        tvm.accept();
        destroyAddress = msg.sender;
        if (protection < attack_power)
        {
            livesCount = livesCount - attack_power + protection ;
        }
        if (IsDead())
        {
            ProccessDeath();
        }
    }

    function IsDead() public returns(bool)
    {
        tvm.accept();
        return livesCount <=0;
    }

    function ProccessDeath() virtual public 
    {
        tvm.accept();
        Destroy();
    }

    function Destroy() public
    {
        destroyAddress.transfer(1, true, 160);
    }


}