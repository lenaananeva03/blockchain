pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

import 'GameObject.sol';

contract BaseStation is GameObject {
    GameObject[] public militaryUnitsArray;

    constructor(uint curProtection, uint curLivesCount) GameObject(curProtection, curLivesCount) public {
    }

    function AddMilitaryUnit(address MilitaryUnitAddress) public
    {
        tvm.accept();
        //push gameObject 
        militaryUnitsArray.push();
    }

    function GetUnitsArray() public returns(GameObject[])
    {
        return militaryUnitsArray;
    }

    function RemoveMilitaryUnit(address militaryUnitAddress) public
    {
        tvm.accept();
        for (uint8 i=0;i<militaryUnitsArray.length;i++)
        {
            if (militaryUnitsArray[i] == militaryUnitAddress)
            {
                delete militaryUnitsArray[i];
            }
        }
    }

    function ProccessDeath() public override
    {
        tvm.accept();
        for (uint8 i = 0; i < militaryUnitsArray.length;i++)
        {
            militaryUnitsArray[i].ProccessDeath();
        }
        this.Destroy();
    }
}