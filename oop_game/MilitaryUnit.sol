pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

import 'GameObject.sol';
import 'GameObjectInterface.sol';
import 'BaseStation.sol';

contract MilitaryUnit is GameObject {
    BaseStation baseStation;    
    uint private attack = 5;

    constructor(BaseStation curBaseStation, uint curProtection, uint curLivesCount, uint curAttackPower) GameObject(curProtection, curLivesCount) public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        curBaseStation.AddMilitaryUnit(address(this));
        baseStation = curBaseStation;
    }

    function Attack(address enemyAddress) public
    {
        tvm.accept();
        //принять атаку для enemyAddress)
        uint power = GetAttackPower();
        // GameObjectInterface.AcceptAttack(power)
    }    

    function GetAttackPower() public returns(uint)
    {
        tvm.accept();
        return attack;
    }

    function ProccessDeath() public override 
    {
        tvm.accept();
        Destroy();
        // delete from list of units
        // GameObject[] arrayUnits = baseStation.militaryUnitsArray;
        // for (uint8 i=0;i<arrayUnits.length;i++)
        // {
        //     if (address(baseStation.GetUnitsArray()[i])==address(this))
        //     {
        //         delete baseStation.GetUnitsArray()[i];
        //     }
        // }
    }

    function DieOfBase() public
    {
        require(msg.sender == address(baseStation), 101);
        tvm.accept();
        ProccessDeath();

    }
}