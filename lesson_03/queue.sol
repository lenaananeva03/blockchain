pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract Queue{
    string[] public queue;
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier accept() {
        tvm.accept();
        _;
    }

    function add(string name) public accept{
        queue.push(name);
    }

    function deleteName() public accept{
        require(!queue.empty(), 103);
        string[] result;
        for(uint8 i=1;i<queue.length;i++)
        {
            result.push(queue[i]);
        }
        queue = result;
    }
}