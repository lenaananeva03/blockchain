pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract taskTracker {
    mapping (uint8=>task) tasks;
    uint8 index=0;

    struct task {
        string name; 
        uint32 timestamp;   
        bool is_done;
    }


    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier accept() {
        tvm.accept();
        _;
    }

    function addTasks(string name) public accept{
        tvm.accept();
        task newTask = task(name, now, false);
        tasks[index]=newTask;
        index++;
    }

    function getOpenTasks() public accept returns (uint) {
        uint count = 0;
        for (uint8 i=0; i<index;i++)
        {
            if (tasks.exists(i))
            {
                task curTask=tasks[i];
                if (!curTask.is_done)
                {
                    count++;
                }            
            }
        }
        return count;
    }

    function getTasks() public accept returns (string[]) {
        string[] tasks_list;
        for (uint8 i=0; i<index;i++)
        {
            if (tasks.exists(i))
            {
                task curTask=tasks[i];
                tasks_list.push(curTask.name);
            }
        }
        return tasks_list;
    }

    function getTaskNameById(uint8 id) public accept returns(string) {
        require(tasks.exists(id), 103);
        return tasks[id].name;
    }

    function deleteTaskById(uint8 id) public accept{
        require(tasks.exists(id), 103);
        delete tasks[id];
    }

    function addDoneTask(uint8 id) public accept{
        require(tasks.exists(id), 103);
        tasks[id].is_done=true;
    }
}
