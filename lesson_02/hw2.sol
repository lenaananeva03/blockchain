pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract numbersMultiplication{
    uint public multiplication = 1;
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    function multiply(uint number) public
    {
        bool condition = number >= 1 && number <=10;
        require(condition, 101);
        tvm.accept();
        multiplication *= number;
    }
}